# Jira importer

Import issues and epics from Jira into GitLab. Include labels, weight, due dates and epic relationships. User mapping from Jira to GitLab users is supported for for assignees.

## Usage

* Fork or clone this repo
* Configure a **protected, masked** CI/CD variable `GIT_TOKEN`, containing a GitLab access token with `api` scope. The token needs at least `reporter` on the group containing the target project (in order to create epics).
* modify (or upload your own) `[jira_importfile.tsv](./jira_importfile.tsv)`. Look at the example file in this repo to understand the desired format.
* modify (or upload your own) `[usermapping.csv](./usermapping.csv)`. Format is simply `jira_username,gitlab_username`. This is used to map assignees of Jira issues to GitLab users
* navigate to CI/CD->Pipelines and click `Run pipeline`. Modify the variables to point to your import and mapping files.
* set the variable `RUN` to `true`.
* press `Run pipeline`

## Script

* load the Jira import file
* load the user mapping file. Resolve GitLab usernames to GitLab user IDs
* find all Epics (`type=="epic"`) in the issue file. For each Epic:
  * Create an epic in the target project's parent group
  * Label it with `status`, `weight`, `label` and `id`
  * set the `due_date` if it exists
  * IF no epic exists that already is labelled with the `id` (this is done so that multiple projects can be imported into a group without creating duplicate epics)
* find all issues (`type!="epic"`) in the issue file. For each issue:
  * Create an issue in the target project
  * Label it with `status`, `label` and `type`
  * set the `due_date` and `weight`
  * assign it to the GitLab users of the `assignees`
  * add it to its epic
* the script logs all objects it creates in a `created_objects.json` artifact

## Run locally

`python jira-importer.py $GITLAB_URL $GIT_TOKEN $PROJECT_ID $IMPORTFILE --user_mapping_file $USERMAPPING`

Parameters:

* `GITLAB_URL`: URL of the GitLab instance to import to
* `GIT_TOKEN`: Token able to create epics in the project's parent group
* `PROJECT_ID`: ID of the project to import into
* `IMPORTFILE`: Tab separated file containing the Jira issues to import
* `USERMAPPING`: Optional file mapping Jira users to GitLab users

# Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request.

This script writes data into GitLab via the API. It observes rate limits, but the data could potentially be difficult to clean up. It logs its write actions to a file, which should allow you to bulk delete if needed.