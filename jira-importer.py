import gitlab
import argparse
import csv
import json
from datetime import datetime

class JiraImporter(object):

    gl_url = ""
    token = ""
    jira_issues = []
    user_mapping = {}
    project = ""
    created_objects = []

    def __init__(self, args):
        self.gl_url = args.gitlab
        self.token = args.token
        self.project = args.project
        self.jira_issues = self.load_issues(args.import_file)
        if args.user_mapping_file:
            self.user_mapping = self.import_usermap(args.user_mapping_file)

    def import_usermap(self, mappingfile):
        print("Loading user mapping")
        gl = gitlab.Gitlab(self.gl_url, private_token = self.token)
        user_mapping = {}
        with open(mappingfile,"r") as mappingfile:
            reader = csv.reader(mappingfile)
            header = [field.lower() for field in next(reader)]
            target_header = ["jira_user","gitlab_user"]
            if header == target_header:
                for row in reader:
                    if len(row) != 2:
                        print('ERROR: Invalid row %s. Each row needs exactly 2 fields.' % row)
                    gitlab_user = gl.users.list(username=row[1])
                    if gitlab_user:
                        user_mapping[row[0]] = gitlab_user[0]
                    else:
                        print('ERROR: Could not find GitLab user %s' % row[1])
            else:
                print('ERROR: User mapping format incorrect: Expecting %s' % target_header)
                exit(1)
        return user_mapping

    def load_issues(self, issuefile):
        print("Loading Jira issues")
        issues = []
        with open(issuefile,"r") as issuefile:
            reader = csv.reader(issuefile, delimiter="\t")
            header = [field.lower() for field in next(reader)]
            target_header = ["id","title","type","description","status","assignees","epic","due date","milestone","weight","label"]
            if header == target_header:
                for row in reader:
                    issue = {}
                    for index, field in enumerate(row):
                        issue[header[index]] = field
                    issues.append(issue)
            else:
                print('ERROR: Jira import file format incorrect: Expecting %s' % target_header)
                exit(1)
        return issues

    def run(self):
        gl = gitlab.Gitlab(self.gl_url, private_token = self.token)
        project = None
        group = None
        try:
            project = gl.projects.get(self.project)
            group = gl.groups.get(project.namespace["id"], with_projects = False)
        except Exception as e:
            print("ERROR: Can not retrieve target project")
            print(e)
            exit(1)
        epics = []
        issues = []
        for jira_issue in self.jira_issues:
            if jira_issue["type"].lower() == "epic":
                epics.append(jira_issue)
            else:
                issues.append(jira_issue)
        epic_objects = self.import_epics(epics, group)
        self.import_issues(issues, epic_objects, project)

    def import_epics(self, epics, group):
        epic_objects = {}
        for epic in epics:
            epic_object = self.add_epic(epic, group)
            epic_objects[epic["id"]] = epic_object
        return epic_objects

    def add_epic(self, epic, group):
        # Labelling with ID to be able to retrieve by ID for issue addition later
        existing_epic = group.epics.list(labels=epic["id"])
        if existing_epic:
            print("Found epic %s in %s: %s" % (epic["id"], group.full_path, existing_epic[0].iid))
            return existing_epic[0]
        else:
            labels = epic["label"].split(",")
            # TODO: treat closed status as "state"
            labels.append("workflow::" + epic["status"])
            labels.append(epic["id"])
            if epic["weight"]:
                labels.append("weight::" + epic["weight"])
            epic_data = {"title": epic["title"],
                        "description": epic["description"],
                        "labels":",".join(labels)}
            if epic["due date"]:
                try:
                    datetime.strptime(epic["due date"], '%Y-%m-%d')
                    epic_data["due_date_fixed"] = epic["due date"]
                    epic_data["due_date_is_fixed"] = True
                except:
                    print("WARN: Epic %s: Could not parse due date %s. Expecting yyyy-mm-dd" % (epic["id"],epic["due date"]))
            print("Creating epic %s in %s" % (epic["id"],group.full_path))
            # TODO: how to handle epic assignee? label?
            try:
                epic_object = group.epics.create(epic_data)
                self.log_object(epic_object, "epic")
                return epic_object
            except Exception as e:
                print("ERROR: Could not create epic")
                print(e)

    def import_issues(self, issues, epic_objects, project):
        for issue in issues:
            labels = issue["label"].split(",")
            labels.append("workflow::" + issue["status"])
            #TODO: Blocked status is a link in GitLab and requires the blocking issue
            labels.append("type::" + issue["type"])
            issue_data = {"title": issue["title"],
                        "description": issue["description"],
                        "labels":",".join(labels)}
            if issue["weight"]:
                issue_data["weight"] = int(issue["weight"])
            if issue["due date"]:
                try:
                    datetime.strptime(issue["due date"], '%Y-%m-%d')
                    issue_data["due_date"] = issue["due date"]
                except:
                    print("WARN: Issue %s: Could not parse due date %s. Expecting yyyy-mm-dd" % (issue["id"],issue["due date"]))
            if issue["assignees"]:
                assignees = issue["assignees"].split(",")
                assignee_ids = []
                for assignee in assignees:
                    if assignee in self.user_mapping:
                        assignee_ids.append(self.user_mapping[assignee].id)
                    else:
                        print("ERROR: Jira assignee not in user map: %s" % assignee)
                issue_data["assignee_ids"] = assignee_ids
            print("Creating issue %s in %s" % (issue["id"],project.path_with_namespace))
            try:
                issue_object = project.issues.create(issue_data)
                if issue["epic"] in epic_objects:
                    epic_object = epic_objects[issue["epic"]]
                    print("Adding issue %s to epic %s" % (issue["id"], issue["epic"]))
                    epic_object.issues.create({"issue_id":issue_object.id})
                self.log_object(issue_object, "issue")
            except Exception as e:
                print("ERROR: Could not create issue")
                print(e)

    def log_object(self, gitlab_object, object_type):
        created_object = {"type": object_type}
        if object_type == "epic":
            created_object["namespace"] = gitlab_object.attributes["_links"]["group"]
        else:
            created_object["namespace"] = gitlab_object.attributes["_links"]["project"]
        created_object["url"] = gitlab_object.web_url
        self.created_objects.append(created_object)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Import issues into jira')
    parser.add_argument('gitlab', help='GitLab URL')
    parser.add_argument('token', help='API token able to create issues in the requested project and its parent group')
    parser.add_argument('project', help='Project ID to import issues to')
    parser.add_argument('import_file', help='CSV file containing Jira issues')
    parser.add_argument('--user_mapping_file', help='File containing mapping of Jira users to GitLab users')
    args = parser.parse_args()
    importer = JiraImporter(args)
    importer.run()

    with open("created_objects.json","w") as logfile:
        json.dump(importer.created_objects, logfile, indent=2)